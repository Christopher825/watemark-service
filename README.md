### Watermark Service Backend ###

# Development technologies #

* JDK 1.8.X_XX
* Spring Boot v1.4.0.RELEASE
* Spring v4.3.2.RELEASE
* Spring Boot AOP
* Spring Boot Test
* Spring Boot Web which run embedded tomcat instance
* Embedded Redis Server 0.6
* Spring Boot Data Redis
* Gradle plugin v1.4.0.RELEASE
* Gradle v3.2.1

#Run test and service#

* Go to root directory of the project and execute below command:-

* Execute command in Unix environment[./gradlew -Penv=test cleanTest test --tests --info  com.publisher.watermark.controller.WatermarkControllerTest bootRun]

* Execute command in Windows environment[gradlew.bat -Penv=test cleanTest test --tests --info  com.publisher.watermark.controller.WatermarkControllerTest bootRun]

#Run test only#

* Go to root directory of the project and execute below command:-

* Execute command in Unix environment[./gradlew -Penv=test cleanTest test --tests --info  com.publisher.watermark.controller.WatermarkControllerTest]

* Execute command in Windows environment[gradlew.bat -Penv=test cleanTest test --tests --info  com.publisher.watermark.controller.WatermarkControllerTest]

# Who do I talk to? #

* Admin : Christopher Loganathan
* Email Contact : christopherloganathan@gmail.com