package com.publisher.watermark.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import redis.embedded.RedisServer;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Embedded cache db Redis for keeping state of data in-memory storage.
 */
@Configuration
public class RedisEmbeddedSrvConfig {

    private RedisServer rs;

    private static final Logger logger = LoggerFactory.getLogger(RedisEmbeddedSrvConfig.class);

    @PostConstruct
    public void startRedisServer() throws IOException {
        rs = new RedisServer();//default port 6379
        rs.start();
        logger.info("Redis Embedded Server started!!!");
    }

    @PreDestroy
    public void stopRedisServer() {
        rs.stop();
    }

}
