package com.publisher.watermark;


import com.publisher.watermark.util.UtilFunc;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;


/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Boot Application of Watermark Service
 */
@SpringBootApplication
public class WatermarkServiceInit extends SpringBootServletInitializer {

    private static final Logger logger = LoggerFactory.getLogger(WatermarkServiceInit.class);

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder springApplicationBuilder) {

        String configPath = System.getProperty("watermark.home");
        String configEnv = System.getProperty("watermark.env");
        if(!UtilFunc.isEmpty(configEnv) &&  !UtilFunc.isEmpty(configPath)) {
            logger.info("Watermark service config path: {}", configPath);
            logger.info("Watermark service config env: {}", configEnv);
            springApplicationBuilder.properties("spring.config.location=".concat(configPath +
                    "application-" +
                    configEnv +
                    ".properties"));
        }
        return springApplicationBuilder.sources(WatermarkServiceInit.class);
    }

    public static void main(String... args) {

        SpringApplication.run(WatermarkServiceInit.class, args);

    }
}
