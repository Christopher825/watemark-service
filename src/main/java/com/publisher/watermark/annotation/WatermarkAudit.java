package com.publisher.watermark.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Audit annotation.
 */

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface WatermarkAudit {

    String value();
}