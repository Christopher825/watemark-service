package com.publisher.watermark.worker;

import com.publisher.watermark.AbstractComponent;
import com.publisher.watermark.annotation.WatermarkAudit;
import com.publisher.watermark.model.Watermark;
import com.publisher.watermark.model.WatermarkBuilder;
import com.publisher.watermark.model.WatermarkStatus;
import com.publisher.watermark.service.WatermarkService;
import com.publisher.watermark.util.CustomUtilScheduleExecutor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import javax.annotation.PreDestroy;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Watermark core processing startup event.
 */

@Component
public class WatermarkProcessStartUpEvent extends AbstractComponent
        implements ApplicationListener<ApplicationReadyEvent> {

    private static final Logger logger = LoggerFactory.getLogger(WatermarkProcessStartUpEvent.class);

    private static ScheduledThreadPoolExecutor scheduledThreadPoolExecutor;

    @Autowired
    private WatermarkService watermarkService;

    @Value("${pool.size}")
    int poolSize;

    @WatermarkAudit("Starting core processing watermark service")
    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        WatermarkProcessStartUpEvent.scheduledThreadPoolExecutor = new CustomUtilScheduleExecutor(poolSize);
        scheduledThreadPoolExecutor .scheduleAtFixedRate(()
                        -> processDocuments(),
                0, 1, TimeUnit.SECONDS);
    }

    private void processDocuments(){
        List<WatermarkBuilder> watermarkBuilders = watermarkService.allDoc();

        watermarkBuilders.stream().filter(watermarkBuilder ->
                watermarkBuilder.getWatermarkStatus() == WatermarkStatus.WAITING_STATE).
                forEach(watermarkBuilder -> {
                    try {
                        watermarkBuilder.getWatermark().generateWatermarkState(new WatermarkProcess(watermarkBuilder));
                    } catch (UnsupportedEncodingException e) {
                        watermarkService.updateDoc(new WatermarkBuilder.Builder()
                                .watermark(watermarkBuilder.getWatermark())
                                .Id(watermarkBuilder.getId())
                                .watermarkStatus(WatermarkStatus.INCOMPLETE)
                                .errRef(errorHandler(e))
                                .build());
                    }
                });
    }

    private class WatermarkProcess implements Watermark.WatermarkProcessNotify {

        private final WatermarkBuilder watermarkBuilder;

        WatermarkProcess(WatermarkBuilder watermarkBuilder) {
            this.watermarkBuilder = watermarkBuilder;
        }


        @Override
        public void success() throws UnsupportedEncodingException {

            Watermark watermarkBuilderWatermark = watermarkBuilder.getWatermark();
            Map<Object,String> watermark = new HashMap<>();
            watermark.put(watermarkBuilder.getId(),
                    UUID.nameUUIDFromBytes(String.valueOf(watermarkBuilder.getId()).getBytes("UTF-8")).toString());
                    watermarkBuilderWatermark.add(watermark);
            watermarkService.updateDoc(new WatermarkBuilder.Builder()
                    .watermark(watermarkBuilderWatermark)
                    .Id(watermarkBuilder.getId())
                    .watermarkStatus(WatermarkStatus.COMPLETE)
                    .build());
            logger.debug("Success : {}",watermarkBuilder.getId());
        }
    }

    @WatermarkAudit("Terminating the core processing watermark service")
    @PreDestroy
    private void shutdownThreadPoolExecutor(){
        scheduledThreadPoolExecutor.shutdown();
    }
}
