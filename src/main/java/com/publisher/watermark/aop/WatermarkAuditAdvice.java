package com.publisher.watermark.aop;

import com.publisher.watermark.annotation.WatermarkAudit;
import com.publisher.watermark.util.UtilException;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.Map;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: AOP advice handler for info and error audit purposes.
 */

@Component
@Aspect
public class WatermarkAuditAdvice {

    private static final Logger logger = LoggerFactory.getLogger(WatermarkAuditAdvice.class);



    @Before("(execution(* com.publisher.watermark.controller.*Controller.*(..)) || " +
            "execution(* com.publisher.watermark.worker.*Event.*(..))) " +
            "&& @annotation(watermarkAudit)")
    public void auditWatermark(WatermarkAudit watermarkAudit) {

            logger.info(watermarkAudit.value());

    }

    @AfterReturning(
            pointcut = "execution(* com.publisher.watermark.error.ErrorHandler.handler(..))",
            returning = "errorStack")
    public void logAfterReturningCaughtException(Map<String,Exception> errorStack){
        UtilException.handleCaughtException(errorStack.entrySet().
                iterator().next().getKey(),errorStack.entrySet().
                iterator().next().getValue());

    }

}

