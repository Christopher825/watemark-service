package com.publisher.watermark.controller;

import com.publisher.watermark.AbstractComponent;
import com.publisher.watermark.annotation.WatermarkAudit;
import com.publisher.watermark.error.CustomException;
import com.publisher.watermark.model.BaseDoc;
import com.publisher.watermark.model.Doc;
import com.publisher.watermark.model.DocType;
import com.publisher.watermark.model.WatermarkStatus;
import com.publisher.watermark.model.dto.DocumentDTO;
import com.publisher.watermark.model.dto.ResponseObj;
import com.publisher.watermark.service.WatermarkService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Watermark Rest API Handler
 */
@RestController
public class WatermarkController extends AbstractComponent {


    private static final Logger logger = LoggerFactory.getLogger(WatermarkController.class);

    @Autowired
    private WatermarkService watermarkService;



    @RequestMapping(value = "/submitDoc",method = RequestMethod.POST)
    @WatermarkAudit("Submit document request")
    public ResponseEntity<ResponseObj> submitDoc(
            @RequestBody DocumentDTO documentDTO) throws CustomException {

        List<String> docTypes = Stream.of(DocType.values())
                .map(Enum::name)
                .collect(Collectors.toList());

        if(docTypes.contains(documentDTO.getContent())){
            BaseDoc baseDoc = new BaseDoc();
            baseDoc.setTitle(documentDTO.getTitle());
            baseDoc.setAuthor(documentDTO.getAuthor());
            Doc doc = new Doc();
            doc.setTopic(documentDTO.getTopic());
            baseDoc.setContent(doc);
            baseDoc.setDocType(DocType.book);
            long id = watermarkService.submitDoc(baseDoc);
            ResponseObj responseObj = new ResponseObj.Builder()
                    .payload(id)
                    .reason("Successful document submission to watermark service")
                    .build();
            return new ResponseEntity<>(responseObj, HttpStatus.OK);
        }else throw new CustomException("This document content type not valid at the moment");

    }

    @RequestMapping(value = "/getDocStatus/{id}",method = RequestMethod.GET)
    @WatermarkAudit("Request status of the submitted document")
    public
    ResponseEntity<ResponseObj> getDocStatus(@PathVariable long id){

        WatermarkStatus watermarkStatus = watermarkService.pollDoc(id);
        ResponseObj responseObj = new ResponseObj.Builder()
                .payload(watermarkStatus.getStatus())
                .build();
        logger.debug("Document id :{}, Status :{}",id,watermarkStatus.getStatus());
        return new ResponseEntity<>(responseObj, HttpStatus.OK);

    }

}
