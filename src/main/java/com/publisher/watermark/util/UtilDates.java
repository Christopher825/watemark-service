package com.publisher.watermark.util;

import java.time.Instant;


/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Date util
 */
public class UtilDates {


    public static long currentEpochTimestamp(){
       return Instant.now().toEpochMilli();
    }

}
