package com.publisher.watermark.util;


import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Custom util time frame executor
 */
public class CustomUtilScheduleExecutor extends ScheduledThreadPoolExecutor {

    public CustomUtilScheduleExecutor(int corePoolSize) {
        super(corePoolSize);
    }

    @Override
    public ScheduledFuture scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return super.scheduleAtFixedRate(runnableWrapper(command), initialDelay, period, unit);
    }

    @Override
    public ScheduledFuture scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return super.scheduleWithFixedDelay(runnableWrapper(command), initialDelay, delay, unit);
    }

    private Runnable runnableWrapper(Runnable command) {
        return new ExceptionRunnableLogging(command);
    }

    private class ExceptionRunnableLogging implements Runnable {
        private Runnable runnable;

        public ExceptionRunnableLogging(Runnable runnable) {
            super();
            this.runnable = runnable;
        }

        @Override
        public void run() {
            try {
                runnable.run();
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}