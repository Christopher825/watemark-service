package com.publisher.watermark.util;

import com.publisher.watermark.error.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: AOP exception for warn and error logger
 */
public class UtilException {

    private static final Logger logger = LoggerFactory.getLogger(UtilException.class);

    public static final void handleCaughtException(String errorRef,Exception exception) {


        if (exception instanceof CustomException)
            logger.warn("Warn ref:[{}]",errorRef,exception);
        else
            logger.error("Error ref:[{}]",errorRef,exception);

    }
}
