package com.publisher.watermark.dao;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Abstract hash implementation for watermark db storage
 */
public abstract class AbstractDao {

    @Autowired
    protected RedisTemplate redisTemplate;

    protected enum PrefixBean {

        WatermarkHashes("#watermark#");

        @Getter
        String prefix;

        PrefixBean(String prefix) {
            this.prefix = prefix;
        }
    }
}

