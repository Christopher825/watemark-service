package com.publisher.watermark.dao;

import com.publisher.watermark.error.CustomException;
import com.publisher.watermark.model.BaseDoc;
import com.publisher.watermark.model.Watermark;
import com.publisher.watermark.model.WatermarkBuilder;
import com.publisher.watermark.model.WatermarkStatus;
import com.publisher.watermark.util.UtilFunc;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.stereotype.Repository;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Watermark dao service
 */

@Repository
public class WatermarkDaoImpl extends AbstractDao implements WatermarkDao {

    /**
     * Submit document for watermark processing
     *
     * @param doc
     * @return unique job processing id
     */
    @Override
    public long submitDoc(BaseDoc doc) {

        HashOperations<String,Long, WatermarkBuilder> ops = redisTemplate.opsForHash();
        WatermarkBuilder watermarkBuilder = new WatermarkBuilder.Builder()
                    .watermark(doc)
                    .Id(doc.getId())
                    .watermarkStatus(WatermarkStatus.WAITING_STATE)
                    .build();

        ops.put(PrefixBean.WatermarkHashes.getPrefix(),doc.getId(), watermarkBuilder);
        return watermarkBuilder.getId();
    }


    /**
     * Updating watermark builder job process status
     *
     * @param watermarkBuilder
     * @return void
     */
    @Override
    public void updateDoc(WatermarkBuilder watermarkBuilder) {

        HashOperations<String,Long, WatermarkBuilder> ops = redisTemplate.opsForHash();
        ops.put(PrefixBean.WatermarkHashes.getPrefix(), watermarkBuilder.getId(),watermarkBuilder);
    }


    /**
     * To check periodically the watermarking process of a document
     *
     * @param id
     * @return status info of job processing.
     */
    @Override
    public WatermarkStatus pollDoc(long id) {

        HashOperations<String,Long, WatermarkBuilder> ops = redisTemplate.opsForHash();
        WatermarkBuilder watermarkBuilder  = ops.get(PrefixBean.WatermarkHashes.getPrefix(),id);
        if (UtilFunc.isEmpty(watermarkBuilder))
            return WatermarkStatus.TICKET_NOT_FOUND;
        return watermarkBuilder.getWatermarkStatus();
    }

    /**
     *To check the status of the document processing or validity of the job id
     *
     * @param id
     * @return Watermark builder document
     * @throws CustomException if wrong job process id
     */
    @Override
    public Watermark searchDoc(long id) throws CustomException {

        HashOperations<String,Long, WatermarkBuilder> ops = redisTemplate.opsForHash();
        WatermarkBuilder watermarkBuilder  = ops.get(PrefixBean.WatermarkHashes.getPrefix(),id);
        if (UtilFunc.isEmpty(watermarkBuilder))
            throw new CustomException(WatermarkStatus.TICKET_NOT_FOUND.getStatus());
        return watermarkBuilder.getWatermark();
    }


    /**
     * Retrieve list of all the document
     *
     * @return list of all document
     *
     */
    @Override
    public List<WatermarkBuilder> allDoc() {
        HashOperations<String,Long, WatermarkBuilder> ops = redisTemplate.opsForHash();
        return ops.entries(PrefixBean.WatermarkHashes.getPrefix()).entrySet()
                .stream()
                .map(m -> m.getValue())
                .collect(Collectors.toList());
    }
}
