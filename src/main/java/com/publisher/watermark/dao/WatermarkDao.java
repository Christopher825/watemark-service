package com.publisher.watermark.dao;

import com.publisher.watermark.error.CustomException;
import com.publisher.watermark.model.Watermark;
import com.publisher.watermark.model.BaseDoc;
import com.publisher.watermark.model.WatermarkBuilder;
import com.publisher.watermark.model.WatermarkStatus;
import java.util.List;


/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Implementation of watermark dao service
 */
public interface WatermarkDao {

    long submitDoc(BaseDoc doc);

    void updateDoc(WatermarkBuilder watermarkBuilder);

    WatermarkStatus pollDoc(long id);

    Watermark searchDoc(long id) throws CustomException;

    List<WatermarkBuilder> allDoc();
}



