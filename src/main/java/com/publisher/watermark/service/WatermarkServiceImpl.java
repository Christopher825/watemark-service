package com.publisher.watermark.service;

import com.publisher.watermark.dao.WatermarkDao;
import com.publisher.watermark.error.CustomException;
import com.publisher.watermark.model.BaseDoc;
import com.publisher.watermark.model.Watermark;
import com.publisher.watermark.model.WatermarkBuilder;
import com.publisher.watermark.model.WatermarkStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

/**
 * Created by christopherloganathan on 21/02/2017.
 */

@Service
public class WatermarkServiceImpl implements WatermarkService{

    @Autowired
    private WatermarkDao watermarkDao;

    @Override
    public long submitDoc(BaseDoc doc) {
        return watermarkDao.submitDoc(doc);
    }

    @Override
    public void updateDoc(WatermarkBuilder watermarkBuilder) {
        watermarkDao.updateDoc(watermarkBuilder);
    }

    @Override
    public WatermarkStatus pollDoc(long id) {
        return watermarkDao.pollDoc(id);
    }

    @Override
    public Watermark searchDoc(long id) throws CustomException {
        return watermarkDao.searchDoc(id);
    }

    @Override
    public List<WatermarkBuilder> allDoc() {
        return watermarkDao.allDoc();
    }
}
