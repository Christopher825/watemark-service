package com.publisher.watermark;

import com.publisher.watermark.error.ErrorHandler;
import com.publisher.watermark.util.UtilDates;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;


/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Abstract component for util purposes.
 */
public abstract class AbstractComponent {


    @Autowired
    private ErrorHandler errorHandler;

    @Autowired
    protected Environment environment;

    protected String errorHandler(Exception exception) {
        String errorRef = Long.toHexString(Double.doubleToLongBits(UtilDates.currentEpochTimestamp()));
        errorHandler.handler(errorRef,exception);
        return errorRef;
    }
}
