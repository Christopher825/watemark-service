package com.publisher.watermark.error;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Custom exception usage
 */
public class CustomException extends Exception {

    public CustomException(String message) {
        super(message);
    }
}
