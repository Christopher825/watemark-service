package com.publisher.watermark.error;

import org.springframework.stereotype.Component;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Error handler component
 */
@Component
public class ErrorHandler {

    public Map<String,Exception> handler(String errorRef, Exception exception){
        Map<String,Exception> error = new HashMap<>();
        error.put(errorRef,exception);
        return error;
    }
}

