package com.publisher.watermark.error;

import com.publisher.watermark.AbstractComponent;
import com.publisher.watermark.model.dto.ResponseObj;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Rest API exception advice
 */
@ControllerAdvice
public class ExceptionAdvice extends AbstractComponent {


    @ExceptionHandler(CustomException.class)
    public ResponseEntity<ResponseObj> handleCustomException(CustomException customException) {

        ResponseObj responseObj = new ResponseObj.Builder()
                .payload(customException.getMessage())
                .reason(errorHandler(customException))
                .build();
        return new ResponseEntity<>(responseObj, HttpStatus.UNPROCESSABLE_ENTITY);

    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<ResponseObj> handleException(Exception exception) {

        ResponseObj responseObj = new ResponseObj.Builder()
                .payload(errorHandler(exception))
                .reason("Internal Error")
                .build();
        return new ResponseEntity<>(responseObj, HttpStatus.INTERNAL_SERVER_ERROR);

    }

}
