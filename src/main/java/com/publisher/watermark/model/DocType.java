package com.publisher.watermark.model;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Doc type
 */
public enum DocType {

    book,
    journal
}
