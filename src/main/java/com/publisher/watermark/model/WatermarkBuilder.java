package com.publisher.watermark.model;
import lombok.Getter;
import java.io.Serializable;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Watermark obj builder
 */
public final class WatermarkBuilder implements Serializable{

    private static final long serialVersionUID = 2002646418147324781L;
    @Getter
    private final Watermark watermark;
    @Getter
    private final WatermarkStatus watermarkStatus;
    @Getter
    private final String errRef;
    @Getter
    private final long id;


    public static class Builder {

        private Watermark watermark;
        private  WatermarkStatus watermarkStatus;
        private String errRef;
        private long id;

        public Builder watermark(Watermark watermark) {
            this.watermark = watermark;
            return this;
        }

        public Builder watermarkStatus(WatermarkStatus watermarkStatus) {
            this.watermarkStatus = watermarkStatus;
            return this;
        }

        public Builder errRef(String errRef) {
            this.errRef = errRef;
            return this;
        }


        public Builder Id(long id) {
            this.id = id;
            return this;
        }

        public WatermarkBuilder build() {
            return new WatermarkBuilder(this);
        }

    }
    private WatermarkBuilder(Builder builder) {
        this.watermark = builder.watermark;
        this.watermarkStatus = builder.watermarkStatus;
        this.errRef = builder.errRef;
        this.id = builder.id;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(this.getClass().getName()).append(" [")
                .append("Id=").append(id).append(", ")
                .append("watermark=").append(watermark).append(", ")
                .append("watermarkStatus=").append(watermarkStatus).append(", ")
                .append("errRef=").append(errRef)
                .append("]");

        return buffer.toString();
    }
}
