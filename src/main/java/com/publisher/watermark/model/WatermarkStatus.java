package com.publisher.watermark.model;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Watermark builder enum status
 */
public enum WatermarkStatus {

    TICKET_NOT_FOUND("Invalid ticket"),
    WAITING_STATE("Looking for available thread from the thread pool"),
    COMPLETE("Successfully processed the document"),
    INCOMPLETE("Not successfully processed the document");

    @Getter@Setter
    String status;

    WatermarkStatus(String status) {
        this.status = status;

    }
}
