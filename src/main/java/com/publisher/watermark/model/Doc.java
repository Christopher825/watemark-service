package com.publisher.watermark.model;

import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Doc details model
 */
public final class Doc implements Serializable {

    private static final long serialVersionUID = -8209706867581464083L;
    @Getter
    @Setter
    private String topic;

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(this.getClass().getName()).append(" [")
                .append("topic=").append(topic)
                .append("]");

        return buffer.toString();
    }
}
