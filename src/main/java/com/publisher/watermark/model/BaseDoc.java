package com.publisher.watermark.model;

import com.publisher.watermark.util.UtilDates;
import lombok.Getter;
import lombok.Setter;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Base document with any document type.
 */
public final class BaseDoc<D> implements Serializable,Cloneable,Watermark {


    private static final long serialVersionUID = 3096445081656098189L;
    @Getter
    @Setter protected long Id = UtilDates.currentEpochTimestamp();
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String author;

    @Getter
    @Setter
    private DocType docType;

    @Getter@Setter private
    D content;

    private Map<Object, String> watermark;


    @Override
    public void generateWatermarkState(WatermarkProcessNotify watermarkProcessNotify)
            throws UnsupportedEncodingException {
        watermarkProcessNotify.success();
    }

    @Override
    public void add(Map<Object, String> watermark) {
        this.watermark = watermark;
    }

    @Override
    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return this;
        }

    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(this.getClass().getName()).append(" [")
                .append("Id=").append(Id).append(", ")
                .append("author=").append(author).append(", ")
                .append("docType=").append(docType).append(", ")
                .append("content=").append(content).append(", ")
                .append("watermark=").append(watermark)
                .append("]");

        return buffer.toString();
    }
}
