package com.publisher.watermark.model;

import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Watermark workflow model implementation
 */
public interface Watermark {

    void generateWatermarkState(WatermarkProcessNotify watermarkProcessNotify) throws UnsupportedEncodingException;

    void add(Map<Object, String> watermark);

    interface WatermarkProcessNotify {

       void success() throws UnsupportedEncodingException;

    }

}
