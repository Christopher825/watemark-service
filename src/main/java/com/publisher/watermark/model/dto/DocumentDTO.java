package com.publisher.watermark.model.dto;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Document dto mapper
 */
public final class DocumentDTO {

    @Getter
    @Setter
    private String content;
    @Getter
    @Setter
    private String title;
    @Getter
    @Setter
    private String author;
    @Getter
    @Setter
    private String topic;

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(this.getClass().getName()).append(" [")
                .append("content=").append(content).append(", ")
                .append("title=").append(title).append(", ")
                .append("author=").append(author).append(", ")
                .append("author=").append(topic)
                .append("]");

        return buffer.toString();
    }

}
