package com.publisher.watermark.model.dto;

import lombok.Getter;


/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: Response obj dto builder
 */
public final class ResponseObj {

    @Getter private final Object payload;
    @Getter private final String reason;

    public static class Builder {

        private  Object payload;
        private  String reason;

        public Builder payload(Object payload) {
            this.payload = payload;
            return this;
        }

        public Builder reason(String reason) {
            this.reason = reason;
            return this;
        }
        public ResponseObj build() {
            return new ResponseObj(this);
        }

    }
    private ResponseObj(Builder builder) {
        this.payload = builder.payload;
        this.reason = builder.reason;
    }

    @Override
    public String toString() {
        StringBuffer buffer = new StringBuffer();

        buffer.append(this.getClass().getName()).append(" [")
                .append("payload=").append(payload).append(", ")
                .append("reason=").append(reason)
                .append("]");

        return buffer.toString();
    }
}
