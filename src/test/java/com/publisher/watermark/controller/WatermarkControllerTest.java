package com.publisher.watermark.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.publisher.watermark.WatermarkServiceInit;
import com.publisher.watermark.model.WatermarkStatus;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import java.util.HashMap;
import java.util.Map;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by christopherloganathan on 21/02/2017.
 * Purpose: MVC Mock test on Watermark Service REST API handler
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = WatermarkServiceInit.class,webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class WatermarkControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private WebApplicationContext webApplicationContext;

    private static final Logger logger = LoggerFactory.getLogger(WatermarkControllerTest.class);

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

    }

    @Test
    public void t1submitDocWithValidContent() throws Exception {

        logger.debug("t1submitDocWithValidContent() -- Starting");
        mockMvc.perform(MockMvcRequestBuilders.post("/submitDoc")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"content\":\"journal\",\n" +
                        "\t\"title\":\"Journal of human flight routes\",\n" +
                        "\t\"author\":\"Clark Kent\"\n"+
                        "}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.reason").value("Successful document submission to watermark service"))
                .andDo(print());
    }

    @Test
    public void t2submitDocWithInValidContent() throws Exception {

        logger.debug("t2submitDocWithInValidContent() -- Starting");
        mockMvc.perform(MockMvcRequestBuilders.post("/submitDoc")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"content\":\"magazine\",\n" +
                        "\t\"title\":\"Football Stars\",\n" +
                        "\t\"author\":\"Chris\", \n" +
                        "\t\"topic\":\"Sports\"\n" +
                        "}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andExpect(jsonPath("$.payload").value("This document content type not valid at the moment"))
                .andDo(print());
    }

    @Test
    public void t3verifyDocWithValidTicketID() throws Exception {

        logger.debug("t3verifyDocWithValidTicketID() -- Starting");

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/submitDoc")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"content\":\"book\",\n" +
                        "\t\"title\":\"How to make money\",\n" +
                        "\t\"author\":\"Dr. Evil\", \n" +
                        "\t\"topic\":\"Business\"\n" +
                        "}")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andDo(print())
                .andReturn();

        Map<String, String> responsePayload = objectMapper.
                readValue(mvcResult.getResponse().getContentAsString(),
                        new TypeReference<HashMap<String, String>>() {
                        });

            Thread.sleep(1000);
            mockMvc.perform(MockMvcRequestBuilders.get("/getDocStatus/"+responsePayload.get("payload"))
                    .accept(MediaType.APPLICATION_JSON))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.payload").value(WatermarkStatus.COMPLETE.getStatus()))
                    .andDo(print());


    }

    @Test
    public void t4verifyDocWithValidTicketID() throws Exception {

        logger.debug("t4verifyDocWithValidTicketID() -- Starting");

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/submitDoc")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\n" +
                        "\t\"content\":\"book\",\n" +
                        "\t\"title\":\"The Dark Code\",\n" +
                        "\t\"author\":\"Bruce Wayne\", \n" +
                        "\t\"topic\":\"Science\"\n" +
                        "}")
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andReturn();

        Map<String, String> responsePayload = objectMapper.
                readValue(mvcResult.getResponse().getContentAsString(),
                        new TypeReference<HashMap<String, String>>() {
                        });

        logger.debug(responsePayload.toString());

        mockMvc.perform(MockMvcRequestBuilders.get("/getDocStatus/"+responsePayload.get("payload"))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload").value(WatermarkStatus.WAITING_STATE.getStatus()))
                .andDo(print());


    }

    @Test
    public void t5verifyDocWithInValidTicketID() throws Exception {

        logger.debug("t5verifyDocWithInValidTicketID() -- Starting");
        long testTicket=1234567890;
        mockMvc.perform(MockMvcRequestBuilders.get("/getDocStatus/"+testTicket)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.payload").value(WatermarkStatus.TICKET_NOT_FOUND.getStatus()))
                .andDo(print());
    }

    @Test
    public void t6verifyDocWithInValidTicketID() throws Exception {

        logger.debug("t6verifyDocWithInValidTicketID() -- Starting");
        String testTicket= "1234567890ABCD";
        mockMvc.perform(MockMvcRequestBuilders.get("/getDocStatus/"+testTicket)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isInternalServerError())
                .andExpect(jsonPath("$.reason").value("Internal Error"))
                .andDo(print());
    }



}
